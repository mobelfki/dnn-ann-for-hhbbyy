#!/usr/bin/env python

import ROOT 
import uproot as pr
import pandas as pd
import numpy as np
from argparse import ArgumentParser
import sys

def getArgs():
	"""
	Get arguments from command line.
	"""
	args = ArgumentParser(description="Argumetns for NTupleToNumpy for ANN")
	args.add_argument('-s', '--samples', action='store', required=True, help='Samples name to process .txt splitted by ,')
	args.add_argument('-i', '--inputdir', action='store', default='/afs/cern.ch/work/m/mobelfki/ANN/NTuple/output', help='Input Directory')
	args.add_argument('-f', '--features', action='store', required=True, help='Features list')
	args.add_argument('-o', '--outdir', action='store', default='output', help='Output Directory')
	return args.parse_args()

class NTupleToNumpy:
	"""
	Produce numpy arrays form NTuple class 
	The output is saved in Array/output directory
	Doesn't take any inputs 
	"""

	def __init__(self, name):
		self.name = name;
		print('NTupleToNumpy', name)
	
	def loadTree(self, path, tree):
		self.Tree = pr.open(path)[tree];
		
	def loadFeatures(self, txt):
		with open(txt,'r') as features_file:
			self.Features = features_file.read().splitlines();
		features_file.close();

	def setLabel(self, label):
		self.Label = label;

	def Execute(self):
		
		df = pd.DataFrame(self.Tree.arrays(self.Features, namedecode="utf-8"));
		
		df = df[self.Features]
		
		df['b1.pt'] = df['b1.pt'] / df['bb.m'];
		df['b2.pt'] = df['b2.pt'] / df['bb.m'];
		df['bb.pt'] = df['bb.pt'] / df['bb.m'];
		
		df['y1.pt'] = df['y1.pt'] / df['yy.m'];
		df['y2.pt'] = df['y2.pt'] / df['yy.m'];
		df['yy.pt'] = df['yy.pt'] / df['yy.m'];

		sumW = df['Event.TotWeight'].sum()

		a = np.ones((df.shape[0],1)) * sumW
		d = pd.DataFrame({'sumW': a[:, 0]})

		df['sumW'] = d

		df['isHH']  = self.Label[0];
		df['isttH'] = self.Label[1];
		df['isZH']  = self.Label[2];
		df['isJJ']  = self.Label[3];

		#df = df.drop(['bb.m'], axis=1)
	
		return df;

def main():
	"""
	The main function of NTupleToNumpy
	"""
	args=getArgs()
	with open(args.samples,'r') as samples_file:
		samples_name = samples_file.read().splitlines();
	samples_file.close();
	features_file = args.features
	trees = {'Tree','Tree_Train','Tree_Val','Tree_Test'}
	for sample in samples_name:
		
		NTuple2Numpy = NTupleToNumpy(sample);
		for tree in trees:
			NTuple2Numpy.loadTree(args.inputdir+'/'+sample,tree);
			NTuple2Numpy.loadFeatures(features_file);
			label = np.array([-1,-1,-1,-1]);
			
			if "hh_yybb" in sample:
				label = np.array([1,0,0,0]);
			if "ZH125J" in sample:
				label = np.array([0,0,1,0]);
			if "PowhegPy8_ggZH125" in sample:
				label = np.array([0,0,1,0]);	
			if "PowhegPy8_ttH125" in sample:
				label = np.array([0,1,0,0]);
			if "aMCnloHwpp_tWH125" in sample:
				label = np.array([0,1,0,0]);
			if "Sherpa2_diphoton" in sample:
				label = np.array([0,0,0,1]);
			if "Sherpa2_diphoton" in sample:
				label = np.array([0,0,0,1]);
			if "PowhegPy8_NNLOPS_ggH125" in sample:
				label = np.array([0,0,0,1]);
			if "PowhegPy8_bbH125" in sample:
				label = np.array([0,0,0,1]);
				
			if label.all() == np.array([-1,-1,-1,-1]).all():
				print("The sample that you give is not setted --check the code--", sample)
				quit()	
		
			NTuple2Numpy.setLabel(label);
	
			out = NTuple2Numpy.Execute();
		
			np.save(args.outdir+'/'+sample+'.'+tree+'.npy', out.to_numpy())
		
if __name__ == '__main__':
	main()
		
