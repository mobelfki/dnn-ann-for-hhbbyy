
{
	
	TFile* _file_HH = new TFile("plot.aMCnloHwpp_hh_yybb_AF2.MxAODDetailed.h024.root","READ");
	TFile* _file_ttH = new TFile("plot.PowhegPy8_ttH125.MxAODDetailed.h024.root","READ");
	TFile* _file_yy = new TFile("plot.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.h024.root","READ");
	TFile* _file_ZH = new TFile("plot.PowhegPy8_ZH125J.MxAODDetailed.h024.root","READ");
	//TFile* _file_bbH = new TFile("plot.PowhegPy8_bbH125.MxAODDetailed.h024.root","READ");

	TH1F* histos_HT_HH[19];
	TH1F* histos_HT_ttH[19];
	TH1F* histos_HT_yy[19];
	TH1F* histos_HT_ZH[19];
	//TH1F* histos_HT_bbH[19];

	TH1F* histos_HP_HH[19];
	TH1F* histos_HP_ttH[19];
	TH1F* histos_HP_yy[19];
	TH1F* histos_HP_ZH[19];
	//TH1F* histos_HP_bbH[19];

	TH1F* histos_HS_HH[19];
	TH1F* histos_HS_ttH[19];
	TH1F* histos_HS_yy[19];
	TH1F* histos_HS_ZH[19];
	//TH1F* histos_HS_bbH[19];

	TH1F* histos_H1_HH[19];
	TH1F* histos_H1_ttH[19];
	TH1F* histos_H1_yy[19];
	TH1F* histos_H1_ZH[19];
	//TH1F* histos_H1_bbH[19];

	TH1F* histos_HY_HH[19];
	TH1F* histos_HY_ttH[19];
	TH1F* histos_HY_yy[19];
	TH1F* histos_HY_ZH[19];
	//TH1F* histos_HY_bbH[19];

	for(int i = 0; i<19; i++)
	{
		histos_HT_HH[i] = (TH1F*)_file_HH->Get(Form("histo_HT_l%i",i+1));
		histos_HT_ttH[i] = (TH1F*)_file_ttH->Get(Form("histo_HT_l%i",i+1));
		histos_HT_yy[i] = (TH1F*)_file_yy->Get(Form("histo_HT_l%i",i+1));
		histos_HT_ZH[i] = (TH1F*)_file_ZH->Get(Form("histo_HT_l%i",i+1));
		//histos_HT_bbH[i] = (TH1F*)_file_bbH->Get(Form("histo_HT_l%i",i+1));

		histos_HP_HH[i] = (TH1F*)_file_HH->Get(Form("histo_HP_l%i",i+1));
		histos_HP_ttH[i] = (TH1F*)_file_ttH->Get(Form("histo_HP_l%i",i+1));
		histos_HP_yy[i] = (TH1F*)_file_yy->Get(Form("histo_HP_l%i",i+1));
		histos_HP_ZH[i] = (TH1F*)_file_ZH->Get(Form("histo_HP_l%i",i+1));
		//histos_HP_bbH[i] = (TH1F*)_file_bbH->Get(Form("histo_HP_l%i",i+1));

		histos_HS_HH[i] = (TH1F*)_file_HH->Get(Form("histo_HS_l%i",i+1));
		histos_HS_ttH[i] = (TH1F*)_file_ttH->Get(Form("histo_HS_l%i",i+1));
		histos_HS_yy[i] = (TH1F*)_file_yy->Get(Form("histo_HS_l%i",i+1));
		histos_HS_ZH[i] = (TH1F*)_file_ZH->Get(Form("histo_HS_l%i",i+1));
		//histos_HS_bbH[i] = (TH1F*)_file_bbH->Get(Form("histo_HS_l%i",i+1));

		histos_H1_HH[i] = (TH1F*)_file_HH->Get(Form("histo_H1_l%i",i+1));
		histos_H1_ttH[i] = (TH1F*)_file_ttH->Get(Form("histo_H1_l%i",i+1));
		histos_H1_yy[i] = (TH1F*)_file_yy->Get(Form("histo_H1_l%i",i+1));
		histos_H1_ZH[i] = (TH1F*)_file_ZH->Get(Form("histo_H1_l%i",i+1));
		//histos_H1_bbH[i] = (TH1F*)_file_bbH->Get(Form("histo_H1_l%i",i+1));

		histos_HY_HH[i] = (TH1F*)_file_HH->Get(Form("histo_HY_l%i",i+1));
		histos_HY_ttH[i] = (TH1F*)_file_ttH->Get(Form("histo_HY_l%i",i+1));
		histos_HY_yy[i] = (TH1F*)_file_yy->Get(Form("histo_HY_l%i",i+1));
		histos_HY_ZH[i] = (TH1F*)_file_ZH->Get(Form("histo_HY_l%i",i+1));
		//histos_HY_bbH[i] = (TH1F*)_file_bbH->Get(Form("histo_HY_l%i",i+1));
	}

	for(int i = 0; i<19; i++)
	{
		histos_HT_HH[i]->SetTitle(Form("HT_l%i",i+1));

		histos_HT_HH[i]->SetLineColor(4);
		histos_HT_ttH[i]->SetLineColor(2);
		histos_HT_yy[i]->SetLineColor(6);
		histos_HT_ZH[i]->SetLineColor(1);
		//histos_HT_bbH[i]->SetLineColor(3);

		histos_HT_HH[i]->SetLineWidth(2);
		histos_HT_ttH[i]->SetLineWidth(2);
		histos_HT_yy[i]->SetLineWidth(2);
		histos_HT_ZH[i]->SetLineWidth(2);
		//histos_HT_bbH[i]->SetLineWidth(2);
		
		histos_HT_HH[i]->SetStats(0);
		histos_HT_ttH[i]->SetStats(0);
		histos_HT_yy[i]->SetStats(0);
		histos_HT_ZH[i]->SetStats(0);
		//histos_HT_bbH[i]->SetStats(0);

		histos_HP_HH[i]->SetTitle(Form("HP_l%i",i+1));
		histos_HP_HH[i]->SetLineColor(4);
		histos_HP_ttH[i]->SetLineColor(2);
		histos_HP_yy[i]->SetLineColor(6);
		histos_HP_ZH[i]->SetLineColor(1);
		//histos_HP_bbH[i]->SetLineColor(3);

		histos_HP_HH[i]->SetLineWidth(2);
		histos_HP_ttH[i]->SetLineWidth(2);
		histos_HP_yy[i]->SetLineWidth(2);
		histos_HP_ZH[i]->SetLineWidth(2);
		//histos_HP_bbH[i]->SetLineWidth(2);
		
		histos_HP_HH[i]->SetStats(0);
		histos_HP_ttH[i]->SetStats(0);
		histos_HP_yy[i]->SetStats(0);
		histos_HP_ZH[i]->SetStats(0);
		//histos_HP_bbH[i]->SetStats(0);

		histos_HS_HH[i]->SetTitle(Form("HS_l%i",i+1));
		histos_HS_HH[i]->SetLineColor(4);
		histos_HS_ttH[i]->SetLineColor(2);
		histos_HS_yy[i]->SetLineColor(6);
		histos_HS_ZH[i]->SetLineColor(1);
		//histos_HS_bbH[i]->SetLineColor(3);

		histos_HS_HH[i]->SetLineWidth(2);
		histos_HS_ttH[i]->SetLineWidth(2);
		histos_HS_yy[i]->SetLineWidth(2);
		histos_HS_ZH[i]->SetLineWidth(2);
		//histos_HS_bbH[i]->SetLineWidth(2);
		
		histos_HS_HH[i]->SetStats(0);
		histos_HS_ttH[i]->SetStats(0);
		histos_HS_yy[i]->SetStats(0);
		histos_HS_ZH[i]->SetStats(0);
		//histos_HS_bbH[i]->SetStats(0);

		histos_H1_HH[i]->SetTitle(Form("H1_l%i",i+1));
		histos_H1_HH[i]->SetLineColor(4);
		histos_H1_ttH[i]->SetLineColor(2);
		histos_H1_yy[i]->SetLineColor(6);
		histos_H1_ZH[i]->SetLineColor(1);
		//histos_H1_bbH[i]->SetLineColor(3);

		histos_H1_HH[i]->SetLineWidth(2);
		histos_H1_ttH[i]->SetLineWidth(2);
		histos_H1_yy[i]->SetLineWidth(2);
		histos_H1_ZH[i]->SetLineWidth(2);
		//histos_H1_bbH[i]->SetLineWidth(2);
		
		histos_H1_HH[i]->SetStats(0);
		histos_H1_ttH[i]->SetStats(0);
		histos_H1_yy[i]->SetStats(0);
		histos_H1_ZH[i]->SetStats(0);
		//histos_H1_bbH[i]->SetStats(0);
		
		histos_HY_HH[i]->SetTitle(Form("HY_l%i",i+1));
		histos_HY_HH[i]->SetLineColor(4);
		histos_HY_ttH[i]->SetLineColor(2);
		histos_HY_yy[i]->SetLineColor(6);
		histos_HY_ZH[i]->SetLineColor(1);
		//histos_HY_bbH[i]->SetLineColor(3);

		histos_HY_HH[i]->SetLineWidth(2);
		histos_HY_ttH[i]->SetLineWidth(2);
		histos_HY_yy[i]->SetLineWidth(2);
		histos_HY_ZH[i]->SetLineWidth(2);
		//histos_HY_bbH[i]->SetLineWidth(2);
		
		histos_HY_HH[i]->SetStats(0);
		histos_HY_ttH[i]->SetStats(0);
		histos_HY_yy[i]->SetStats(0);
		histos_HY_ZH[i]->SetStats(0);
		//histos_HY_bbH[i]->SetStats(0);

		histos_HT_HH[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_HT_ttH[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_HT_yy[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_HT_ZH[i]->GetYaxis()->SetRangeUser(0.,0.1);

		histos_HP_HH[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_HP_ttH[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_HP_yy[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_HP_ZH[i]->GetYaxis()->SetRangeUser(0.,0.1);

		histos_HY_HH[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_HY_ttH[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_HY_yy[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_HY_ZH[i]->GetYaxis()->SetRangeUser(0.,0.1);

		histos_HS_HH[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_HS_ttH[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_HS_yy[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_HS_ZH[i]->GetYaxis()->SetRangeUser(0.,0.1);

		histos_H1_HH[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_H1_ttH[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_H1_yy[i]->GetYaxis()->SetRangeUser(0.,0.1);
		histos_H1_ZH[i]->GetYaxis()->SetRangeUser(0.,0.1);
	}
	
	auto legend = new TLegend(0.1,0.8,0.2,0.9);
	legend->AddEntry(histos_HT_HH[0],"HH");
	legend->AddEntry(histos_HT_ttH[0],"ttH");
	legend->AddEntry(histos_HT_yy[0],"yyjj");
	legend->AddEntry(histos_HT_ZH[0],"ZH");
	//legend->AddEntry(histos_HT_bbH[0],"bbH");

	TCanvas* c_HT[19];
	TCanvas* c_HP[19];
	TCanvas* c_HS[19];
	TCanvas* c_H1[19];
	TCanvas* c_HY[19];

	for(int i = 0; i<19; i++)
	{
		c_HT[i] = new TCanvas(Form("c_HT_l%i",i+1),"",750,750,750,750);
		
		histos_HT_HH[i]->Draw();
		histos_HT_ttH[i]->Draw("same");
		histos_HT_ZH[i]->Draw("same");
		//histos_HT_bbH[i]->Draw("same");
		histos_HT_yy[i]->Draw("same");
		legend->Draw("same");
		
		c_HT[i]->SaveAs(Form("img/HT_l%i.png",i+1));	
		
		if( i == 0)
		{
			c_HT[i]->Print("img/plots.pdf(","pdf");
		}else{
			c_HT[i]->Print("img/plots.pdf","pdf");
		}
	}

	for(int i = 0; i<19; i++)
	{
		c_HP[i] = new TCanvas(Form("c_HP_l%i",i+1),"",750,750,750,750);
		
		histos_HP_HH[i]->Draw();
		histos_HP_ttH[i]->Draw("same");
		histos_HP_ZH[i]->Draw("same");
		//histos_HP_bbH[i]->Draw("same");
		histos_HP_yy[i]->Draw("same");
		legend->Draw("same");
	
		c_HP[i]->Print("img/plots.pdf","pdf");
		
		c_HP[i]->SaveAs(Form("img/HP_l%i.png",i+1));	
	}

	for(int i = 0; i<19; i++)
	{
		c_HS[i] = new TCanvas(Form("c_HS_l%i",i+1),"",750,750,750,750);
		
		histos_HS_HH[i]->Draw();
		histos_HS_ttH[i]->Draw("same");
		histos_HS_ZH[i]->Draw("same");
		//histos_HS_bbH[i]->Draw("same");
		histos_HS_yy[i]->Draw("same");
		legend->Draw("same");
		c_HS[i]->Print("img/plots.pdf","pdf");
		c_HS[i]->SaveAs(Form("img/HS_l%i.png",i+1));	
	}

	for(int i = 0; i<19; i++)
	{
		c_H1[i] = new TCanvas(Form("c_H1_l%i",i+1),"",750,750,750,750);
		
		histos_H1_HH[i]->Draw();
		histos_H1_ttH[i]->Draw("same");
		histos_H1_ZH[i]->Draw("same");
		//histos_H1_bbH[i]->Draw("same");
		histos_H1_yy[i]->Draw("same");
		legend->Draw("same");
		c_H1[i]->Print("img/plots.pdf","pdf");
		c_H1[i]->SaveAs(Form("img/H1_l%i.png",i+1));		
	}

	for(int i = 0; i<19; i++)
	{
		c_HY[i] = new TCanvas(Form("c_HY_l%i",i+1),"",750,750,750,750);
		
		histos_HY_HH[i]->Draw();
		histos_HY_ttH[i]->Draw("same");
		histos_HY_ZH[i]->Draw("same");
		//histos_HY_bbH[i]->Draw("same");
		histos_HY_yy[i]->Draw("same");
		legend->Draw("same");
		c_HY[i]->Print("img/plots.pdf","pdf");		
		c_HY[i]->SaveAs(Form("img/HY_l%i.png",i+1));		
	}
	c_HY[0]->Print("img/plots.pdf)","pdf");
	
}
