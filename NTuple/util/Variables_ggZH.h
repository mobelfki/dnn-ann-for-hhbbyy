#include <memory>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <iostream>

// ROOT include(s):
#include <TFile.h>
#include <TH1F.h>
#include <TChain.h>
#include <TError.h>
#include <TString.h>
#include <TStopwatch.h>
#include <TSystem.h>
#include "TLorentzVector.h"
#include "TRandom3.h"

bool _isDirectory = false;
Int_t _nSample = 1;
Int_t _nCompain = 3;
TString _GlobalPath = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/"; 
TString _Compain [] = {"mc16a","mc16d","mc16e"};
TString _Tag[] = {"e5762_s3126_r9364_p3665","e5762_s3126_r10201_p3665","e5762_s3126_r10724_p3665"};
TString _Type = ".MxAODDetailed";
TString _Version = ".h024";
TString _Samplename[]  = {".PowhegPy8_ggZH125"};
TString _histo_name = "CutFlow_PowhegPy8_ggZH125_noDalitz_weighted";
