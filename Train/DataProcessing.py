#!/usr/bin/env python
import torch
import numpy as np
import pandas as pd


class Data:
	"""
	Data class for data pre-processing
	Training type DNN or ANN
	"""

	def __init__(self):
		#self.Type = ttype;
		self.loadGlobalData();
		self.loadTrainData();
		self.loadTestData();
		self.loadValData();
		self.setXYZ();
		self.ProcessData();
		
	def setXYZ(self):
		self.X = self.GlobalData[:,:-3];
		self.Y = self.GlobalData[:,-1:].transpose()[0];
		self.Z = self.GlobalData[:,-3];
		self.W = self.GlobalData[:,-2];
		
		self.X_Train = self.TrainData[:,:-3];
		self.Y_Train = self.TrainData[:,-1:].transpose()[0];
		self.Z_Train = self.TrainData[:,-3];
		self.W_Train = self.TrainData[:,-2];
	
		self.X_Val = self.ValData[:,:-3];
		self.Y_Val = self.ValData[:,-1:].transpose()[0];
		self.Z_Val = self.ValData[:,-3];
		self.W_Val = self.ValData[:,-2];

		self.X_Test = self.TestData[:,:-3];
		self.Y_Test = self.TestData[:,-1:].transpose()[0];
		self.Z_Test = self.TestData[:,-3];
		self.W_Test = self.TestData[:,-2];

	def loadGlobalData(self):
		hh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.aMCnloHwpp_hh_yybb_AF2.MxAODDetailed.h024.root.Tree.npy');
		#tth = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ttH125.MxAODDetailed.h024.root.Tree.npy');
		zh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ZH125J.MxAODDetailed.h024.root.Tree.npy');
		jj  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.h024.root.Tree.npy');

		#hh = np.append(hh, tth, axis=0);
		hh = np.append(hh, zh,  axis=0);
		hh = np.append(hh, jj,  axis=0);

		self.GlobalData = hh;

	def loadTrainData(self):
		hh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.aMCnloHwpp_hh_yybb_AF2.MxAODDetailed.h024.root.Tree_Train.npy');
		#tth = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ttH125.MxAODDetailed.h024.root.Tree_Train.npy');
		zh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ZH125J.MxAODDetailed.h024.root.Tree_Train.npy');
		jj  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.h024.root.Tree_Train.npy');

		#hh = np.append(hh, tth, axis=0);
		hh = np.append(hh, zh,  axis=0);
		hh = np.append(hh, jj,  axis=0);

		self.TrainData = hh;

	def loadTestData(self):
		hh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.aMCnloHwpp_hh_yybb_AF2.MxAODDetailed.h024.root.Tree_Test.npy');
		#tth = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ttH125.MxAODDetailed.h024.root.Tree_Test.npy');
		zh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ZH125J.MxAODDetailed.h024.root.Tree_Test.npy');
		jj  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.h024.root.Tree_Test.npy');

		#hh = np.append(hh, tth, axis=0);
		hh = np.append(hh, zh,  axis=0);
		hh = np.append(hh, jj,  axis=0);

		self.TestData = hh;

	def loadValData(self):
		hh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.aMCnloHwpp_hh_yybb_AF2.MxAODDetailed.h024.root.Tree_Val.npy');
		#tth = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ttH125.MxAODDetailed.h024.root.Tree_Val.npy');
		zh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ZH125J.MxAODDetailed.h024.root.Tree_Val.npy');
		jj  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.h024.root.Tree_Val.npy');

		#hh = np.append(hh, tth, axis=0);
		hh = np.append(hh, zh,  axis=0);
		hh = np.append(hh, jj,  axis=0);

		self.ValData = hh;

	def ProcessData(self):
		
		self.Rescale(self.X_Train)
		self.Rescale(self.X_Val)
		self.Rescale(self.X_Test)

		self.Rescale(self.Z_Train)
		self.Rescale(self.Z_Val)
		self.Rescale(self.Z_Test)
		
		self.Mean = np.mean(self.X_Train, axis=0);
		self.STD  = np.std(self.X_Train, axis=0);
		
		self.X_Train -= self.Mean[None, :];
		self.X_Train /= self.STD[None, :];

		self.X_Val -= self.Mean[None, :];
		self.X_Val /= self.STD[None, :];

		self.X_Test -= self.Mean[None, :];
		self.X_Test /= self.STD[None, :];


		self.Z_Train -= np.mean(self.Z_Train, axis=0);
		self.Z_Train /= np.std(self.Z_Train, axis=0);

		self.Z_Val -= np.mean(self.Z_Train, axis=0);
		self.Z_Val /= np.std(self.Z_Train, axis=0);

		self.Z_Test -= np.mean(self.Z_Train, axis=0);
		self.Z_Test /= np.std(self.Z_Train, axis=0);
	
	def Rescale(self, X):
		
		X_std = (X - X.min(axis=0)) / (X.max(axis=0) - X.min(axis=0))
		X = X_std * (1 - 0) + 0  # (max - min) + min [min,max]
 		

	def getData(self, t):
	
		if t == 'X':
			return self.X;
		if t == 'Y':
			return self.Y;
		if t == 'Z':
			return self.Z;
	
		
