#!/usr/bin/env python
import torch
import numpy as np
import pandas as pd
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
import torch.nn.functional as F
from argparse import ArgumentParser
from torchsummary import summary
from torch.optim.lr_scheduler import StepLR, ReduceLROnPlateau
from torch.utils.data import TensorDataset, DataLoader, random_split
from models import Classifier
from DataProcessing import Data
from Tool import EarlyStopping

data = Data();

def getArgs():
	"""
	Get arguments from command line.
	"""
	args = ArgumentParser(description="Argumetns for training for DNN")
	args.add_argument('-n', '--nepochs', action='store', default=10, help='Number of epochs')
	args.add_argument('-o', '--noutput', action='store', default=4, help='Number of outputs from the model nclass')
	args.add_argument('-l', '--learning_rate', action='store', default=0.1, help='learning rate')
	args.add_argument('-d', '--dropout_rate', action='store', default=0.3, help='Dropout rate')
	args.add_argument('-b', '--batch_size', action='store', default=2000, help='batch size')
	args.add_argument('-N', '--nerouns', action='store', default=256, help='number of neurons in each layer')
	args.add_argument('-B', '--BatchTraining', action='store', default=True, help='Train With batch')
	return args.parse_args()

def compute_loss(Pred,Truth):

	return F.cross_entropy(Pred,Truth).mean();

def train_Batch(N_epochs,n_outputs,drop,learing_rate,_batch_size,_neurons):
	es = EarlyStopping(patience=15, verbose=True)

	X_Train = torch.as_tensor(data.X_Train, dtype=torch.float);
	Y_Train = torch.as_tensor(data.Y_Train, dtype=torch.int64);
	W_Train = torch.as_tensor(data.W_Train, dtype=torch.float);	
	

	X_Val = torch.as_tensor(data.X_Val, dtype=torch.float);
	Y_Val = torch.as_tensor(data.Y_Val, dtype=torch.int64);
	W_Val = torch.as_tensor(data.W_Val, dtype=torch.float);


	dataset_train = TensorDataset(X_Train,Y_Train,W_Train);
	dataset_val = TensorDataset(X_Val,Y_Val,W_Val);

	dataloader_train = DataLoader(dataset_train, batch_size=_batch_size, shuffle=True);
	dataloader_val = DataLoader(dataset_val, batch_size=_batch_size, shuffle=False);


	n_inputs = X_Train.shape[1];
	
	clf = Classifier('DNN_model',n_inputs,n_outputs,drop,_neurons);
	print("The module summary")
	summary(clf, (n_inputs,n_inputs))
	optimiser = optim.Adam(clf.parameters(), lr=learing_rate, weight_decay=3e-5);
	loss_function = nn.CrossEntropyLoss();

	scheduler = StepLR(optimiser, step_size=2, gamma=0.96)

	train_loss = []
	dev_loss = []
	
	avg_train_loss = []
	avg_dev_loss = []

	acc = []
	avg_acc = []

	num_epochs = int(N_epochs)
	iter = 0
	for epoch in range(1, num_epochs + 1):
		scheduler.step()
		clf.train()
		for (X_b, Y_b, W_b) in dataloader_train:
			
			#loss function
			prediction = clf(X_b);
		
			#print(prediction)

			loss = loss_function(prediction, Y_b);

			#loss = loss.cpu()*W_b.cpu()

			#loss = loss.mean();

			#back propagation
			optimiser.zero_grad();
			loss.backward();
			optimiser.step();
			train_loss.append(loss.data.numpy().item());

		clf.eval()
		for (X_v, Y_v, W_v) in dataloader_val:
			logits = clf(torch.as_tensor(X_v, dtype=torch.float));
			
			loss = loss_function(logits, Y_v);

			#loss = loss.cpu()*W_v.cpu()
			#loss = loss.mean()

			dev_loss.append(loss.data.numpy().item());
			Y_p = torch.as_tensor(logits, dtype=torch.int64)
			Y_p = Y_p.max(1)[1].data
			acc.append(np.mean((Y_v == Y_p).numpy()))

		tr_loss = np.average(train_loss)
		dv_loss = np.average(dev_loss)
		accuracy = np.average(acc)

		avg_train_loss.append(tr_loss)
		avg_dev_loss.append(dv_loss)
		avg_acc.append(accuracy)
	
		epoch_len = len(str(N_epochs))
        	print('[ %i / %i ] ## tr_loss: %.5f ## dev_loss: %.5f ## accuracy: %.3f '%(epoch,N_epochs,tr_loss,dv_loss,100*accuracy))
       
		train_loss = []
		dev_loss = []
		es(dv_loss, clf)
	
		if es.early_stop:
            		print("Early stopping")
            		break

	clf.load_state_dict(torch.load('checkpoint.pt'))
	
	return clf, avg_train_loss, avg_dev_loss, avg_acc

def main():
	"""
	The main function of train_DNN
	"""
	args = getArgs();
	noutput = int(args.noutput)
	dropout_rate = float(args.dropout_rate)
	learning_rate = float(args.learning_rate)
	nepochs = int(args.nepochs)
	batch_size = int(args.batch_size)
	n_neurons = int(args.nerouns)
	with_batch = bool(args.BatchTraining)
	if(with_batch):
		print("Training with batch ...")
		model, avg_train_loss, avg_dev_loss, avg_acc = train_Batch(nepochs,noutput,dropout_rate,learning_rate,batch_size,n_neurons)
		model.eval()
		minposs = avg_dev_loss.index(min(avg_dev_loss))+1 
		fig = plt.figure(figsize=(12, 12))
		plt.plot(range(1,len(avg_train_loss)+1), avg_train_loss, label='Training avg loss')
		plt.plot(range(1,len(avg_dev_loss)+1), avg_dev_loss, label='Test avg loss')
		plt.plot(range(1,len(avg_acc)+1), avg_acc, label='Acc avg' )
		plt.axvline(minposs, linestyle='--', color='r',label='Early Stopping Checkpoint')
		plt.grid(True)
		plt.xlabel('epochs')
		plt.legend()
		fig.savefig("Plot/"+model.name+".png")

		print("Testing ... ")
		loss_test = []
		acc_test = []
		X_Test = torch.as_tensor(data.X_Test, dtype=torch.float);
		W_Test = torch.as_tensor(data.W_Test, dtype=torch.float);
		Y_Test = torch.as_tensor(data.Y_Test, dtype=torch.int64);
		dataset_test = TensorDataset(X_Test,Y_Test,W_Test);
		dataloader_test = DataLoader(dataset_test, batch_size=batch_size, shuffle=False);
		loss_function = nn.CrossEntropyLoss();
		for (X_v, Y_v, W_v) in dataloader_test:
			logits = model(torch.as_tensor(X_v, dtype=torch.float));
		
			loss = loss_function(logits, Y_v);

			#loss = loss.cpu()*W_v.cpu()
			#loss = loss.mean()	
	
			loss_test.append(loss.data.numpy().item());	
			Y_p = torch.as_tensor(logits, dtype=torch.int64)
			Y_p = Y_p.max(1)[1].data
			acc_test.append(np.mean((Y_v == Y_p).numpy()))

		print('# Loss : %.5f # Accuracy : %.5f'%(np.mean(loss_test),100*np.mean(acc_test)))
		print('Start Analysis ...')
		d_HH = []
		
		out = model(torch.as_tensor(X_Test, dtype=torch.float));
		
		print(out)

		#d_hh = out[:,0]/(out[:,1]+out[:,2]+out[:,3])

		#d_hh = np.log(d_hh.detach().numpy())
		

		fig2 = plt.figure(figsize=(12, 12))
		plt.hist(out.max(1)[1].data.detach().numpy())
		fig2.savefig("test.png")
	else:
		print("Training without batch ... To be added")
		
		
if __name__== '__main__':
	main()


