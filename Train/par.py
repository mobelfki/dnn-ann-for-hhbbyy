#!/usr/bin/env python36
import torch
import numpy as np
import pandas as pd
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from torchsummary import summary
from torch.optim.lr_scheduler import StepLR
from torch.utils.data import TensorDataset, DataLoader, random_split
from models import Classifier
from DataProcessing import Data
from sklearn.datasets import make_classification
from skorch import NeuralNetClassifier
from sklearn.model_selection import GridSearchCV

def main():
	"""
	The main function of par
	"""
	args = getArgs();
	
	net = NeuralNetClassifier(Classifier, max_epochs=20, lr=0.1, verbose=0, optimizer__momentum=0.9)

if __name__== '__main__':
	main()

	
