#!/usr/bin/env python
import numpy as np
import pandas as pd
import torch 
import torch.nn as nn 
import torch.nn.functional as F

#Classifier class
class Classifier(nn.Module):
	"""
	Classifier class
	
	Args: 
		name : name of the classifier
		n_inputs : number of inputs features
		n_outputs : number of outputs
		drop_rate : dropout rate
	"""

	def __init__(self, name, n_inputs, n_outputs, drop_rate, n_neuros):
		super(Classifier, self).__init__()
		self.name = name;
		self.NInt = n_inputs;
		self.NOut = n_outputs;
		self.nN   = n_neuros;
 		self.DropRate = drop_rate;

		#Input layer 
		self.input = nn.Linear(self.NInt, self.nN);
		#nn.init.xavier_normal_(self.input.weight); # initiatliziation with xavier
	
		#Hidden layers
		self.h1 = nn.Linear(self.nN,self.nN);
		nn.init.xavier_normal_(self.h1.weight);

		self.h2 = nn.Linear(self.nN,self.nN);
		nn.init.xavier_normal_(self.h2.weight);

		self.h3 = nn.Linear(self.nN,self.nN);
		nn.init.xavier_normal_(self.h3.weight);

		self.h4 = nn.Linear(self.nN,self.nN);
		nn.init.xavier_normal_(self.h4.weight);

		#Output layer
		self.output = nn.Linear(self.nN,self.NOut);
		nn.init.xavier_normal_(self.output.weight);

		#Dropout
		self.drop1 = nn.Dropout(self.DropRate);
		self.drop2 = nn.Dropout(self.DropRate);

		self.relu = nn.ReLU()
		self.softmax = nn.Softmax(dim=-1)

	def forward(self, x):
		
		x = F.relu(self.input(x))
		
		x = self.relu(x)

		x = self.h2(x)
		
		x = self.drop1(x)

	#	x = self.h3(x)	
				
	#	x = self.relu(x)

	#	x = self.h4(x)
		
	#	x = self.drop2(x)

		x = self.relu(x)

		x = self.output(x)

		x = self.softmax(x)

		return torch.squeeze(x)	
		#return x

#Regressor Class 
class Regressor(nn.Module):
	"""
	Regressor Class

	Args:
		name      : name of the regressor 
		n_inputs  : number of inputs features
		n_outputs : number of outputs
		drop_rate : dropout rate
	"""

	def __init__(self, name, n_inputs, n_outputs, drop_rate, n_neuros):
		super(Regressor, self).__init__()
		self.name = name;
		self.NInt = n_inputs;
		self.NOut = n_outputs;
		self.nN   = n_neuros;
		self.DropRate = drop_rate;

		#Input layer 
		self.input = nn.Linear(self.NInt, self.nN);
		#nn.init.xavier_normal_(self.input.weight);

		#Hidden layers
		self.h1 = nn.Linear(self.nN,self.nN);
		#nn.init.xavier_normal_(self.h1.weight);

		self.h2 = nn.Linear(self.nN,self.nN);
		#nn.init.xavier_normal_(self.h2.weight);
		
		#Output layer
		self.output = nn.Linear(self.nN,self.NOut);
		#nn.init.xavier_normal_(self.output.weight);

		#Dropout
		self.drop = nn.Dropout(self.DropRate);

	def forward(self, x):
		
		x = F.relu(self.input(x))
		
		x = self.drop(F.relu(self.h1(x)))
		x = F.relu(self.h2(x))
	
		x = self.output(x)

		return x	
	 
	
	
		
