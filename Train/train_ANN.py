#!/usr/bin/env python
import torch
import numpy as np
import pandas as pd
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from torchsummary import summary
from torch.optim.lr_scheduler import StepLR
from torch.utils.data import TensorDataset, DataLoader, random_split
from models import Classifier, Regressor
from DataProcessing import Data

def getArgs():
	"""
	Get arguments from command line.
	"""
	args = ArgumentParser(description="Argumetns for training for ANN")
	args.add_argument('-n', '--nepochs', action='store', default=10, help='Number of epochs')
	args.add_argument('-b', '--batch_size', action='store', default=1000, help='batch size')
	args.add_argument('-Cl', '--Clearning_rate', action='store', default=0.01, help='Classifier learning rate')
	args.add_argument('-Cd', '--Cdropout_rate', action='store', default=0.3, help='Classifier Dropout rate')
	args.add_argument('-CN', '--Cnerouns', action='store', default=512, help='number of neurons in each layer for Classifier')
	args.add_argument('-Rl', '--Rlearning_rate', action='store', default=0.001, help='Regressor learning rate')
	args.add_argument('-Rd', '--Rdropout_rate', action='store', default=0.3, help='Regressor Dropout rate')
	args.add_argument('-RN', '--Rnerouns', action='store', default=128, help='number of neurons in each layer for Regressor')
	args.add_argument('-lam', '--Lambda', action='store', default=10, help='Penality for Regressor Classifier combinition')
	return args.parse_args()

def train(N_epochs, batch_size, clf_lr, clf_dr, clf_nn, adv_lr, adv_dr, adv_nn, lam):
	
	"""

	ANN training 	

	"""
	#Data

	data = Data();

	X_Train = torch.as_tensor(data.X_Train, dtype=torch.float);
	Y_Train = torch.as_tensor(data.Y_Train, dtype=torch.int64);
	Z_Train = torch.as_tensor(data.Z_Train.reshape(-1,1), dtype=torch.float);

	X_Val = torch.as_tensor(data.X_Val, dtype=torch.float);
	Y_Val = torch.as_tensor(data.Y_Val, dtype=torch.int64);
	Z_Val = torch.as_tensor(data.Z_Val.reshape(-1,1), dtype=torch.float);

	dataset_train = TensorDataset(X_Train,Y_Train,Z_Train);
	dataset_val = TensorDataset(X_Val,Y_Val,Z_Val);

	dataloader_train = DataLoader(dataset_train, batch_size=batch_size, pin_memory=True,shuffle=True);
	dataloader_val   = DataLoader(dataset_val, batch_size=batch_size);

	n_inputsX = X_Train.shape[1];
	n_outputs = 4;
	n_outputZ = Z_Train.shape[1];

	#Classifier
	clf = Classifier('ANN_model',n_inputsX,n_outputs,clf_dr,clf_nn);
	clf_loss = nn.CrossEntropyLoss();
	clf_optimiser = optim.Adam(clf.parameters(), lr=clf_lr, weight_decay=1e-4);

	#Regressor

	adv = Regressor('ANN_model',n_outputs,n_outputZ,adv_dr,adv_nn);
	adv_loss = nn.MSELoss();
	adv_optimiser = optim.Adam(adv.parameters(), lr=adv_lr, weight_decay=1e-4);

	clf_losses_train = []
	clf_losses_val   = []	
	adv_losses_train = []
	adv_losses_val   = []
	acc              = []
	clf_train_loss   = []
	clf_val_loss     = []
	plt.figure(figsize=(12, 12))	

	print("The module summary")
	summary(clf, (n_inputsX,n_inputsX))
 	summary(adv,  (n_outputs,n_outputs))
	M_epochs = 1

	for i in range(N_epochs):
		
		clf.train(True);	
		for j in range(M_epochs):
				
			clf_output = clf(X_Train)
			adv_output = adv(clf_output)
			loss_adv      = adv_loss(adv_output, Z_Train);
	
			adv.zero_grad()
			loss_adv.backward()
			adv_optimiser.step()

		adv_losses_train.append(loss_adv.data.numpy().item())

		clf_output = clf(X_Train)
		adv_output = adv(clf_output)
			
		loss_adv = adv_loss(adv_output, Z_Train);
		loss_clf = clf_loss(clf_output, torch.max(Y_Train, 1)[1]);
			
		loss  = loss_clf - lam * loss_adv; 

		clf.zero_grad()
		loss.backward()
		clf_optimiser.step()
		clf_losses_train.append(loss_clf.data.numpy().item());

	clf.train(False);

	logits = clf(torch.as_tensor(X_Val, dtype=torch.float));
	Z_p    = adv(torch.as_tensor(logits, dtype=torch.float));

	clf_losses_val.append(clf_loss(logits, torch.max(Y_Val, 1)[1]).data.numpy().item());
	adv_losses_val.append(adv_loss(Z_p, Z_Val).data.numpy().item());

	Y_p = torch.as_tensor(logits, dtype=torch.int64);
		#Y_v = Y_v.max(1)[1].data
	acc.append(np.mean((Y_Val == Y_p).numpy()))

	print('Epoch : %i  # Clf : Training mean loss : %.5f # Testing mean loss : %.5f # Acc : %.3f '%(i+1,np.mean(clf_losses_train[-len(dataset_train) // batch_size :]),np.mean(clf_losses_val[-len(dataset_val) // batch_size :]), 100*np.mean(acc[-len(dataset_val) // batch_size :])))
	print('Epoch : %i  # Adv : Training mean loss : %.5f # Testing mean loss : %.5f '%(i+1,np.mean(adv_losses_train[-len(dataset_train) // batch_size :]),np.mean(adv_losses_val[-len(dataset_val) // batch_size :])))


		#clf_train_loss.append(np.mean(clf_losses_train))
		#clf_val_loss.append(np.mean(clf_losses_val))

	#plot loss 
	plt.plot(clf_train_loss_mean, label='Training mean loss')
	plt.plot(clf_val_loss_mean, label='Val mean loss')
	plt.legend()
	plt.savefig("Plot/"+clf.name+".png")	

def main():
	"""
	The main function of train_ANN
	"""
	arg = getArgs();
	N_epochs   = int(arg.nepochs)
	batch_size = int(arg.batch_size)
	clf_lr     = float(arg.Clearning_rate)
	clf_dr     = float(arg.Cdropout_rate)
	clf_nn     = int(arg.Cnerouns)

	adv_lr     = float(arg.Rlearning_rate)
	adv_dr     = float(arg.Rdropout_rate)
	adv_nn     = int(arg.Rnerouns)
	lam        = float(arg.Lambda)

	train(N_epochs, batch_size, clf_lr, clf_dr, clf_nn, adv_lr, adv_dr, adv_nn, lam)

if __name__== '__main__':
	main()


