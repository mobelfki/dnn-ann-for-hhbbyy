#!/usr/bin/env python
import torch
import numpy as np
import pandas as pd


class Data:
	"""
	Data class for data pre-processing
	Training type DNN or ANN
	"""

	def __init__(self):
		#self.Type = ttype;
		self.loadGlobalData();
		self.loadTrainData();
		self.loadTestData();
		self.loadValData();
		self.setW();
		self.setXYZ();
		self.ProcessData();
		
	def setW(self):
		
		return
		#self.SetClassWieght(self.GlobalData)
		#self.SetClassWieght(self.TrainData)
		#self.SetClassWieght(self.ValData)

		#self.SetClassWieght(self.TestData)

		
		#self.SetClassWieght(self.HH)
		#self.SetClassWieght(self.ttH)
		#self.SetClassWieght(self.ZH)
		#self.SetClassWieght(self.JJ)

	def setXYZ(self):

		self.X = self.GlobalData[:,:-8];
		self.Y = self.GlobalData[:,-4:];
		self.Z = self.GlobalData[:,-8];
		self.W = self.GlobalData[:,-7];
		
		self.X_Train = self.TrainData[:,:-8];
		self.Y_Train = self.TrainData[:,-4:];
		self.Z_Train = self.TrainData[:,-8];
		self.W_Train = self.TrainData[:,-7];
	
		self.X_Val = self.ValData[:,:-8];
		self.Y_Val = self.ValData[:,-4:];
		self.Z_Val = self.ValData[:,-8];
		self.W_Val = self.ValData[:,-7];

		self.X_Test = self.TestData[:,:-8];
		self.Y_Test = self.TestData[:,-4:];
		self.Z_Test = self.TestData[:,-8];
		self.W_Test = self.TestData[:,-7];

		self.X_HH = self.HH[:,:-8];
		self.Y_HH = self.HH[:,-4:];
		self.Z_HH = self.HH[:,-8];
		self.W_HH = self.HH[:,-7];
		self.C_HH = self.HH[:,-6];

		self.X_ttH = self.ttH[:,:-8];
		self.Y_ttH = self.ttH[:,-4:];
		self.Z_ttH = self.ttH[:,-8];
		self.W_ttH = self.ttH[:,-7];
		self.C_ttH = self.ttH[:,-6]; 
 
		self.X_ZH = self.ZH[:,:-8];
		self.Y_ZH = self.ZH[:,-4:];
		self.Z_ZH = self.ZH[:,-8];
		self.W_ZH = self.ZH[:,-7];
		self.C_ZH = self.ZH[:,-6];

		self.X_JJ = self.JJ[:,:-8];
		self.Y_JJ = self.JJ[:,-4:];
		self.Z_JJ = self.JJ[:,-8];
		self.W_JJ = self.JJ[:,-7];
		self.C_JJ = self.JJ[:,-6];

	def loadGlobalData(self):
		hh   = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.aMCnloHwpp_hh_yybb_AF2.MxAODDetailed.h024.root.Tree.npy');
		tth  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ttH125.MxAODDetailed.h024.root.Tree.npy');
		zh   = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ZH125J.MxAODDetailed.h024.root.Tree.npy');
		jj   = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.h024.root.Tree.npy');
		twh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.aMCnloHwpp_tWH125_yt_plus1.MxAODDetailed.h024.root.Tree.npy');
		ggzh = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ggZH125.MxAODDetailed.h024.root.Tree.npy');
		bbh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_bbH125.MxAODDetailed.h024.root.Tree.npy')
		ggh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_NNLOPS_ggH125.MxAODDetailed.h024.root.Tree.npy');
	
		tth = np.append(tth, twh, axis=0);
		zh = np.append(ggzh, zh,  axis=0);
		jj = np.append(jj, bbh,  axis=0);
		jj = np.append(jj, ggh,  axis=0);

		hh = np.append(hh, tth, axis=0);
		hh = np.append(hh, zh, axis=0);
		hh = np.append(hh, jj, axis=0);

		#self.GlobalData = self.Suffle(hh,tth,zh,jj);

		self.GlobalData = hh;

	def loadTrainData(self):
		hh   = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.aMCnloHwpp_hh_yybb_AF2.MxAODDetailed.h024.root.Tree_Train.npy');
		tth  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ttH125.MxAODDetailed.h024.root.Tree_Train.npy');
		zh   = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ZH125J.MxAODDetailed.h024.root.Tree_Train.npy');
		jj   = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.h024.root.Tree_Train.npy');
		twh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.aMCnloHwpp_tWH125_yt_plus1.MxAODDetailed.h024.root.Tree_Train.npy');
		ggzh = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ggZH125.MxAODDetailed.h024.root.Tree_Train.npy');
		bbh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_bbH125.MxAODDetailed.h024.root.Tree_Train.npy');
		ggh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_NNLOPS_ggH125.MxAODDetailed.h024.root.Tree_Train.npy');

		tth = np.append(tth, twh, axis=0);
		zh = np.append(ggzh, zh,  axis=0);
		jj = np.append(jj, bbh,  axis=0);
		jj = np.append(jj, ggh,  axis=0);

		hh = np.append(hh, tth, axis=0);
		hh = np.append(hh, zh, axis=0);
		hh = np.append(hh, jj, axis=0);

		#self.TrainData = self.Suffle(hh,tth,zh,jj);
		
		self.TrainData = hh;

	def loadTestData(self):
		hh   = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.aMCnloHwpp_hh_yybb_AF2.MxAODDetailed.h024.root.Tree_Test.npy');
		tth  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ttH125.MxAODDetailed.h024.root.Tree_Test.npy');
		zh   = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ZH125J.MxAODDetailed.h024.root.Tree_Test.npy');
		jj   = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.h024.root.Tree_Test.npy');
		#twh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.aMCnloHwpp_tWH125_yt_plus1.MxAODDetailed.h024.root.Tree_Test.npy');
		#ggzh = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ggZH125.MxAODDetailed.h024.root.Tree_Test.npy');
		#bbh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_bbH125.MxAODDetailed.h024.root.Tree_Test.npy');
		#ggh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_NNLOPS_ggH125.MxAODDetailed.h024.root.Tree_Test.npy');


		self.HH  = hh
		self.ttH = tth
		self.ZH  = zh
		self.JJ  = jj

		#tth = np.append(tth, twh, axis=0);
		#zh = np.append(ggzh, zh,  axis=0);
		#jj = np.append(jj, bbh,  axis=0);
		#jj = np.append(jj, ggh,  axis=0);

		hh = np.append(hh, tth, axis=0);
		hh = np.append(hh, zh, axis=0);
		hh = np.append(hh, jj, axis=0);

		#self.TestData = self.Suffle(hh,tth,zh,jj);

		self.TestData = hh;

	def loadValData(self):
		hh   = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.aMCnloHwpp_hh_yybb_AF2.MxAODDetailed.h024.root.Tree_Val.npy');
		tth  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ttH125.MxAODDetailed.h024.root.Tree_Val.npy');
		zh   = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ZH125J.MxAODDetailed.h024.root.Tree_Val.npy');
		jj   = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.h024.root.Tree_Val.npy');
		twh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.aMCnloHwpp_tWH125_yt_plus1.MxAODDetailed.h024.root.Tree_Val.npy');
		ggzh = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_ggZH125.MxAODDetailed.h024.root.Tree_Val.npy');
		bbh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_bbH125.MxAODDetailed.h024.root.Tree_Val.npy');
		ggh  = np.load('/afs/cern.ch/work/m/mobelfki/ANN/Array/output/output.PowhegPy8_NNLOPS_ggH125.MxAODDetailed.h024.root.Tree_Val.npy');

		tth = np.append(tth, twh, axis=0);
		zh = np.append(ggzh, zh,  axis=0);
		jj = np.append(jj, bbh,  axis=0);
		jj = np.append(jj, ggh,  axis=0);

		hh = np.append(hh, tth, axis=0);
		hh = np.append(hh, zh, axis=0);
		hh = np.append(hh, jj, axis=0);

		#self.ValData = self.Suffle(hh,tth,zh,jj);

		self.ValData = hh;

	def ProcessData(self):
		
		#self.Rescale(self.X_Train)
		#self.Rescale(self.X_Val)
		#self.Rescale(self.X_Test)

		#self.Rescale(self.Z_Train)
		#self.Rescale(self.Z_Val)
		#self.Rescale(self.Z_Test)

		#self.Rescale(self.X_HH)
		#self.Rescale(self.Z_HH)

		#self.Rescale(self.X_ttH)
		#self.Rescale(self.Z_ttH)

		#self.Rescale(self.X_ZH)
		#self.Rescale(self.Z_ZH)

		#self.Rescale(self.X_JJ)
		#self.Rescale(self.Z_JJ)
		
		self.Mean = np.mean(self.X_Train, axis=0);
		self.STD  = np.std(self.X_Train, axis=0);
		
		self.X_Train -= self.Mean[None, :];
		self.X_Train /= self.STD[None, :];

		self.X_Val -= self.Mean[None, :];
		self.X_Val /= self.STD[None, :];

		self.X_Test -= self.Mean[None, :];
		self.X_Test /= self.STD[None, :];

		self.X_HH -= self.Mean[None, :];
		self.X_HH /= self.STD[None, :];

		self.X_ZH -= self.Mean[None, :];
		self.X_ZH /= self.STD[None, :];

		self.X_ttH -= self.Mean[None, :];
		self.X_ttH /= self.STD[None, :];

		self.X_JJ -= self.Mean[None, :];
		self.X_JJ /= self.STD[None, :];
		
		"""

		self.Z_Train -= np.mean(self.Z_Train, axis=0);
		self.Z_Train /= np.std(self.Z_Train, axis=0);

		self.Z_Val -= np.mean(self.Z_Train, axis=0);
		self.Z_Val /= np.std(self.Z_Train, axis=0);

		self.Z_Test -= np.mean(self.Z_Train, axis=0);
		self.Z_Test /= np.std(self.Z_Train, axis=0);

		self.Z_HH -= np.mean(self.Z_Train, axis=0);
		self.Z_HH /= np.std(self.Z_Train, axis=0);

		self.Z_ttH -= np.mean(self.Z_Train, axis=0);
		self.Z_ttH /= np.std(self.Z_Train, axis=0);
		
		self.Z_ZH -= np.mean(self.Z_Train, axis=0);
		self.Z_ZH /= np.std(self.Z_Train, axis=0);

		self.Z_JJ -= np.mean(self.Z_Train, axis=0);
		self.Z_JJ /= np.std(self.Z_Train, axis=0);

		"""

	def SetClassWieght(self, X):

		
		#sumW = 0;
		#for row in X:
		#	if row[-4] == 1:
		#		sumW = row[-5]
		#		break
		for row in X:
			if row[-4] == 1:
				row[-6] = row[-6] * 1.
			if row[-3] == 1:
				row[-6] = row[-6] * 0.089
			if row[-2] == 1:
  				row[-6] = row[-6] * 4.913
			if row[-1] == 1:
				row[-6] = row[-6] * 0.8921			
	
		
	def Rescale(self, X):
		
		X_std = (X - X.min(axis=0)) / (X.max(axis=0) - X.min(axis=0))
		X = X_std * (1 - 0) + 0  # (max - min) + min [min,max]

	def Suffle(self, X, Y, Z, W):

		i = 0
		out = np.empty((0,X.shape[1]))
		for row in Z:
			reslt = np.vstack((row,Y[i],Z[i],W[i]))
			out = np.append(out, reslt,axis=0)
			i = i+1

		return out

	def getData(self, t):
	
		if t == 'X':
			return self.X;
		if t == 'Y':
			return self.Y;
		if t == 'Z':
			return self.Z;
		if t == 'HH':
			return self.X_HH;
		if t == 'ttH':
			return self.X_ttH;
		if t == 'ZH':
			return self.X_ZH;
		if t == 'JJ':
			return self.X_JJ;
		
	
		
