#!/usr/bin/env python
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
import keras


#Classifier class 

class Classifier():
	
	"""
	Classifier class
	
	Args: 
		name : name of the classifier
		n_inputs : number of inputs features
		n_outputs : number of outputs
		drop_rate : dropout rate
	"""

	def __init__(self, name, n_inputs, n_outputs, drop_rate, n_neuros):
		
		self.name = name;
		self.NInt = n_inputs;
		self.NOut = n_outputs;
		self.nN   = n_neuros;
 		self.DropRate = drop_rate;

		self.Sequential = Sequential()
		
		self.Sequential.add(Dense(self.nN, input_dim=self.NInt, activation='selu', kernel_initializer=keras.initializers.he_uniform(seed=None), bias_initializer='zeros'));
		self.Sequential.add(Dropout(self.DropRate))	
		self.Sequential.add(Dense(self.nN, activation='selu', kernel_initializer=keras.initializers.he_uniform(seed=None), bias_initializer='zeros'));
		self.Sequential.add(Dropout(self.DropRate))
		self.Sequential.add(Dense(self.nN, activation='selu', kernel_initializer=keras.initializers.he_uniform(seed=None), bias_initializer='zeros'));
		self.Sequential.add(Dropout(self.DropRate))
		self.Sequential.add(Dense(self.NOut, activation='softmax', kernel_initializer=keras.initializers.he_uniform(seed=None), bias_initializer='zeros'));
		
		"""

		self.Sequential.add(Dense(512, input_dim=self.NInt, activation='selu', kernel_initializer=keras.initializers.he_uniform(seed=None), bias_initializer='zeros'));
		self.Sequential.add(Dropout(self.DropRate))
		self.Sequential.add(Dense(512, activation='selu', kernel_initializer=keras.initializers.he_uniform(seed=None), bias_initializer='zeros'));
		self.Sequential.add(Dropout(self.DropRate))
		self.Sequential.add(Dense(64, activation='selu', kernel_initializer=keras.initializers.he_uniform(seed=None), bias_initializer='zeros'));
		self.Sequential.add(Dropout(self.DropRate))
		self.Sequential.add(Dense(self.NOut, activation='softmax', kernel_initializer=keras.initializers.he_uniform(seed=None), bias_initializer='zeros'));
		"""
