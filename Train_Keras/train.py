#!/usr/bin/env python
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from models import Classifier
from DataProcessing import Data
from keras import optimizers
from keras.models import model_from_json
from sklearn.metrics import roc_curve, roc_auc_score, auc
from scipy import interp
from itertools import cycle
from keras.callbacks import EarlyStopping
import os
import math
import ROOT
from ROOT import *
import root_numpy as nr

data = Data();

def getArgs():
	"""
	Get arguments from command line.
	"""
	args = ArgumentParser(description="Argumetns for training for DNN")
	args.add_argument('-n', '--nepochs', action='store', default=10, help='Number of epochs')
	args.add_argument('-o', '--noutput', action='store', default=4, help='Number of outputs from the model nclass')
	args.add_argument('-l', '--learning_rate', action='store', default=0.0001, help='learning rate')
	args.add_argument('-d', '--dropout_rate', action='store', default=0., help='Dropout rate')
	args.add_argument('-b', '--batch_size', action='store', default=250, help='batch size')
	args.add_argument('-N', '--nerouns', action='store', default=256, help='number of neurons in each layer')
	return args.parse_args()

def train(N_epochs,n_outputs,drop,learing_rate,_batch_size,_neurons):
	
	n_inputs = data.X_Train.shape[1];

	
	clf = Classifier('DNN_Classifier',n_inputs,n_outputs,drop,_neurons).Sequential

	adm = optimizers.Adam(lr=learing_rate)
	clf.compile(loss='categorical_crossentropy', optimizer=adm, metrics=['accuracy']);

	es = EarlyStopping(monitor='val_loss', patience=5, verbose=1, mode='min')
	callbacks_list = [es]
	print(clf.summary())

	classweight = [{0: 1, 1: 1}, {0: 1, 1: 0.5343}, {0: 1, 1: 9.9956}, {0: 1, 1: 2.76388}]
	
	history = clf.fit(data.X_Train, data.Y_Train, epochs=N_epochs, batch_size=_batch_size, shuffle=True, callbacks=callbacks_list, validation_data=(data.X_Val,data.Y_Val), class_weight=classweight);

	return clf, history

def plot_roc_curve(Y_true, Y_flase,dirname):
 
	fpr = dict()
	tpr = dict()
	roc_auc = dict()
	for i in range(4):
    		fpr[i], tpr[i], _ = roc_curve(Y_true[:, i], Y_flase[:, i])
    		roc_auc[i] = auc(fpr[i], tpr[i])
	fpr["micro"], tpr["micro"], _ = roc_curve(Y_true.ravel(), Y_flase.ravel())
	roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
	all_fpr = np.unique(np.concatenate([fpr[i] for i in range(4)]))

	# Then interpolate all ROC curves at this points
	mean_tpr = np.zeros_like(all_fpr)
	for i in range(4):
    		mean_tpr += interp(all_fpr, fpr[i], tpr[i])

# Finally average it and compute AUC
	mean_tpr /= 4

	fpr["macro"] = all_fpr
	tpr["macro"] = mean_tpr
	roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

# Plot all ROC curves
	plt.figure()
	#plt.plot(fpr["micro"], tpr["micro"],
        #label='micro-average ROC curve (area = {0:0.2f})'''.format(roc_auc["micro"]), color='deeppink', linestyle=':', linewidth=4)

	#plt.plot(tpr["macro"], fpr["macro"],
        #label='macro-average ROC curve (area = {0:0.2f})'''.format(roc_auc["macro"]),
        #color='navy', linestyle=':', linewidth=4)
	colors = cycle(['aqua', 'darkorange', 'cornflowerblue', 'red'])
	samples = ['HH','ttH','ZH','YY']
	for i, color in zip(range(4), colors):
    		plt.plot(tpr[i], 1-fpr[i], color=color, lw=2, label='ROC curve of class {0} (area = {1:0.2f})'''.format(samples[i], roc_auc[i]))

	#plt.plot([0, 1], [0, 1], 'k--', lw=2)
	plt.xlim([0.0, 1.0])
	plt.ylim([0.0, 1.05])
	plt.xlabel('Signal Eff')
	plt.ylabel('Bkg Rejection')
	plt.title('Some extension of Receiver operating characteristic to multi-class')
	plt.legend(loc="lower right")
	plt.savefig(dirname+"/ROC.png")

def plot_model_history(model_history,dirname):

    fig, axs = plt.subplots(1,2,figsize=(15,5))

    axs[0].plot(range(1,len(model_history.history['acc'])+1),model_history.history['acc'])
    axs[0].plot(range(1,len(model_history.history['val_acc'])+1),model_history.history['val_acc'])
    axs[0].set_title('Model Accuracy')
    axs[0].set_ylabel('Accuracy')
    axs[0].set_xlabel('Epoch')
    axs[0].set_xticks(np.arange(1,len(model_history.history['acc'])+1),len(model_history.history['acc'])/10)
    axs[0].legend(['train', 'val'], loc='best')

    axs[1].plot(range(1,len(model_history.history['loss'])+1),model_history.history['loss'])
    axs[1].plot(range(1,len(model_history.history['val_loss'])+1),model_history.history['val_loss'])
    axs[1].set_title('Model Loss')
    axs[1].set_ylabel('Loss')
    axs[1].set_xlabel('Epoch')
    axs[1].set_xticks(np.arange(1,len(model_history.history['loss'])+1),len(model_history.history['loss'])/10)
    axs[1].legend(['train', 'val'], loc='best')
    fig.savefig(dirname+"/LOSS.png")

def plot_D_HH(HH,ttH,ZH,JJ,dirname):

	pHH_HH  = HH[:,0]
	pHH_ttH = HH[:,1]
	pHH_ZH  = HH[:,2]
	pHH_JJ  = HH[:,3]

	pttH_HH  = ttH[:,0]
	pttH_ttH = ttH[:,1]
	pttH_ZH  = ttH[:,2]
	pttH_JJ  = ttH[:,3]

	pZH_HH  = ZH[:,0]
	pZH_ttH = ZH[:,1]
	pZH_ZH  = ZH[:,2]
	pZH_JJ  = ZH[:,3]

	pJJ_HH  = JJ[:,0]
	pJJ_ttH = JJ[:,1]
	pJJ_ZH  = JJ[:,2]
	pJJ_JJ  = JJ[:,3]

	d_HH   = np.log( pHH_HH  / (pHH_ttH+pHH_ZH+pHH_JJ) )
	d_ttH  = np.log( pttH_HH / (pttH_ttH+pttH_ZH+pttH_JJ) )
	d_ZH   = np.log( pZH_HH  / (pZH_ttH+pZH_ZH+pZH_JJ) )
	d_JJ   = np.log( pJJ_HH  / (pJJ_ttH+pJJ_ZH+pJJ_JJ) )

	plt.figure()
	plt.hist(d_HH, histtype='step', range=(-30,15), bins=45, density=True)
	plt.hist(d_ttH, histtype='step', range=(-30,15), bins=45, density=True)
	plt.hist(d_ZH, histtype='step', range=(-30,15), bins=45, density=True)
	plt.hist(d_JJ, histtype='step', range=(-30,15), bins=45, density=True)
	plt.yscale('log')
	plt.xlabel('D_HH')
	plt.ylabel('A.U')
	plt.legend(['HH','ttH','ZH','JJ'])
	plt.savefig(dirname+"/DHH.png")

def plot_Z(HH,ttH,ZH,JJ,cat,dirname):

	Scale_HH  = [3.117, 4.090, 4.294, 4.303, 4.217];
	Scale_ZH  = [4.177, 4.170, 4.270, 3.957, 4.130];
	Scale_ttH = [4.042, 4.028, 3.970, 4.010, 4.022];
	Scale_JJ  = [3.874, 4.245, 5.172, 3.397, 3.970]; 

	pHH_HH  = HH[:,0]
	pttH_HH = ttH[:,0]
	pZH_HH  = ZH[:,0]
	pJJ_HH  = JJ[:,0]

	X = np.linspace(0., .99, 100)
	Sig = []
	XX = []
	
	Z_max = 0;
	cut = 0;
	for x in X:
		sumW_sig = 0
		sumW_bkg = 0
		for i in range(0,pHH_HH.shape[0]):
			if pHH_HH[i] > x and (data.Z_HH[i]*1e-3 < 130 and data.Z_HH[i]*1e-3 > 120):
				if cat == 5:
					sumW_sig = sumW_sig+data.W_HH[i]*Scale_HH[cat-1]
				elif data.C_HH[i] == cat:
					sumW_sig = sumW_sig+data.W_HH[i]*Scale_HH[cat-1]

		for i in range(0,pttH_HH.shape[0]):
			if pttH_HH[i] > x and (data.Z_ttH[i]*1e-3 < 130 and data.Z_ttH[i]*1e-3 > 120):
				if cat == 5:
					sumW_bkg = sumW_bkg+data.W_ttH[i]*Scale_ttH[cat-1]
				elif data.C_ttH[i] == cat:
					sumW_bkg = sumW_bkg+data.W_ttH[i]*Scale_ttH[cat-1]

		for i in range(0,pZH_HH.shape[0]):
			if pZH_HH[i] > x and (data.Z_ZH[i]*1e-3 < 130 and data.Z_ZH[i]*1e-3 > 120):
				if cat == 5:
					sumW_bkg = sumW_bkg+data.W_ZH[i]*Scale_ZH[cat-1]
				elif data.C_ZH[i] == cat:
					sumW_bkg = sumW_bkg+data.W_ZH[i]*Scale_ZH[cat-1]		
			
		for i in range(0,pJJ_HH.shape[0]):
			if pJJ_HH[i] > x and (data.Z_JJ[i]*1e-3 < 130 and data.Z_JJ[i]*1e-3 > 120):
				if cat == 5:
					sumW_bkg = sumW_bkg+data.W_JJ[i]*Scale_JJ[cat-1]
				elif data.C_JJ[i] == cat:
					sumW_bkg = sumW_bkg+data.W_JJ[i]*Scale_JJ[cat-1]
				
		if sumW_bkg < 0.001:
			break;
		Z = math.sqrt( 2*( (sumW_sig+sumW_bkg)*math.log(1+(sumW_sig/sumW_bkg)) - sumW_sig ) )
		Sig.append(Z)
		XX.append(x)

		if Z > Z_max:
			Z_max = Z
			cut = x
	print('Z Maximum Z : %.3f  for category : %i  Cut : %.3f' %(Z_max, cat, cut) )
	plt.figure()
	plt.plot(XX,Sig)
	plt.xlabel('X')
	plt.ylabel('Z')
	plt.savefig(dirname+"/Z_"+str(cat)+"_.png")
	return Z_max

def plot_Z_DHH(HH,ttH,ZH,JJ,cat,dirname):

	Scale_HH  = [3.117, 4.090, 4.294, 4.303, 4.217];
	Scale_ZH  = [4.177, 4.170, 4.270, 3.957, 4.130];
	Scale_ttH = [4.042, 4.028, 3.970, 4.010, 4.022];
	Scale_JJ  = [3.874, 4.245, 5.172, 3.397, 3.970];

	pHH_HH  = HH[:,0]
	pHH_ttH = HH[:,1]
	pHH_ZH  = HH[:,2]
	pHH_JJ  = HH[:,3]

	pttH_HH  = ttH[:,0]
	pttH_ttH = ttH[:,1]
	pttH_ZH  = ttH[:,2]
	pttH_JJ  = ttH[:,3]

	pZH_HH  = ZH[:,0]
	pZH_ttH = ZH[:,1]
	pZH_ZH  = ZH[:,2]
	pZH_JJ  = ZH[:,3]

	pJJ_HH  = JJ[:,0]
	pJJ_ttH = JJ[:,1]
	pJJ_ZH  = JJ[:,2]
	pJJ_JJ  = JJ[:,3]

	d_HH   = np.log( pHH_HH  / (pHH_ttH+pHH_ZH+pHH_JJ) )
	d_ttH  = np.log( pttH_HH / (pttH_ttH+pttH_ZH+pttH_JJ) )
	d_ZH   = np.log( pZH_HH  / (pZH_ttH+pZH_ZH+pZH_JJ) )
	d_JJ   = np.log( pJJ_HH  / (pJJ_ttH+pJJ_ZH+pJJ_JJ) )

	X = np.linspace(-30, 5, 3500)
	Sig = []
	XX = []

	Z_max = 0;
	cut = 0
	for x in X:
		sumW_sig = 0
		sumW_bkg = 0
		for i in range(0,d_HH.shape[0]):
			if d_HH[i] > x and (data.Z_HH[i]*1e-3 < 130 and data.Z_HH[i]*1e-3 > 120):
				if cat == 5:
					sumW_sig = sumW_sig+data.W_HH[i]*Scale_HH[cat-1];
				elif data.C_HH[i] == cat:
					sumW_sig = sumW_sig+data.W_HH[i]*Scale_HH[cat-1];

		for i in range(0,d_ttH.shape[0]):
			if d_ttH[i] > x and (data.Z_ttH[i]*1e-3 < 130 and data.Z_ttH[i]*1e-3 > 120):
				if cat == 5:
					sumW_bkg = sumW_bkg+data.W_ttH[i]*Scale_ttH[cat-1];
				elif data.C_ttH[i] == cat:
					sumW_bkg = sumW_bkg+data.W_ttH[i]*Scale_ttH[cat-1];

		for i in range(0,d_ZH.shape[0]):
			if d_ZH[i] > x and (data.Z_ZH[i]*1e-3 < 130 and data.Z_ZH[i]*1e-3 > 120):
				if cat == 5:
					sumW_bkg = sumW_bkg+data.W_ZH[i]*Scale_ZH[cat-1];
				elif data.C_ZH[i] == cat:
					sumW_bkg = sumW_bkg+data.W_ZH[i]*Scale_ZH[cat-1];

		for i in range(0,d_JJ.shape[0]):
			if d_JJ[i] > x and (data.Z_JJ[i]*1e-3 < 130 and data.Z_JJ[i]*1e-3 > 120):
				if cat == 5:
					sumW_bkg = sumW_bkg+data.W_JJ[i]*Scale_JJ[cat-1];
				elif data.C_JJ[i] == cat:
					sumW_bkg = sumW_bkg+data.W_JJ[i]*Scale_JJ[cat-1];
		if sumW_bkg < 0.001:
			break;
		Z = math.sqrt( 2*( (sumW_sig+sumW_bkg)*math.log(1+(sumW_sig/sumW_bkg)) - sumW_sig ) )

		if Z > Z_max:
			Z_max = Z;
			cut = x;
		Sig.append(Z)
		XX.append(x)
	
	print('Z_DHH Maximum Z : %.3f  for category %i cut : %.3f' %(Z_max, cat, cut) )
	plt.figure()
	plt.plot(XX,Sig)
	plt.xlabel('D_HH_cut')
	plt.ylabel('Z')
	plt.savefig(dirname+"/Z_DHH_"+str(cat)+"_.png")
		
def main():
	"""
	The main function of train_DNN
	"""
	args = getArgs();
	noutput = int(args.noutput)
	dropout_rate = float(args.dropout_rate)
	learning_rate = float(args.learning_rate)
	nepochs = int(args.nepochs)
	batch_size = int(args.batch_size)
	n_neurons = int(args.nerouns)
	"""
	print('Computing Scale factors ...');
	print('Signal')

	sumW = 0;
	sumW_1 = 0;
	sumW_2 = 0;
	sumW_3 = 0;
	sumW_4 = 0;

	for i in range(0,data.W_HH.shape[0]):
		if (data.Z_HH[i]*1e-3 < 130 and data.Z_HH[i]*1e-3 > 120) :
			sumW = sumW + data.W_HH[i];	
			if data.C_HH[i] == 1:
				sumW_1 = sumW_1 + data.W_HH[i];
			if data.C_HH[i] == 2:
				sumW_2 = sumW_2 + data.W_HH[i];
			if data.C_HH[i] == 3:
				sumW_3 = sumW_3 + data.W_HH[i];
			if data.C_HH[i] == 4:
				sumW_4 = sumW_4 + data.W_HH[i];

	print('Total Yields : %.5f  # Cat1 : %.5f # Cat2 : %.5f # Cat3 : %.5f # Cat4 : %.5f  # Sum : %.5f' % (sumW,sumW_1,sumW_2,sumW_3,sumW_4, sumW_1+sumW_2+sumW_3+sumW_4 ) );

	print('ZH')

	sumW = 0;
	sumW_1 = 0;
	sumW_2 = 0;
	sumW_3 = 0;
	sumW_4 = 0;

	for i in range(0,data.W_ZH.shape[0]):
		if (data.Z_ZH[i]*1e-3 < 130 and data.Z_ZH[i]*1e-3 > 120) :
			sumW = sumW + data.W_ZH[i];	
			if data.C_ZH[i] == 1:
				sumW_1 = sumW_1 + data.W_ZH[i];
			if data.C_ZH[i] == 2:
				sumW_2 = sumW_2 + data.W_ZH[i];
			if data.C_ZH[i] == 3:
				sumW_3 = sumW_3 + data.W_ZH[i];
			if data.C_ZH[i] == 4:
				sumW_4 = sumW_4 + data.W_ZH[i];

	print('Total Yields : %.5f  # Cat1 : %.5f # Cat2 : %.5f # Cat3 : %.5f # Cat4 : %.5f  # Sum : %.5f' % (sumW,sumW_1,sumW_2,sumW_3,sumW_4, sumW_1+sumW_2+sumW_3+sumW_4 ) );

	print('ttH')

	sumW = 0;
	sumW_1 = 0;
	sumW_2 = 0;
	sumW_3 = 0;
	sumW_4 = 0;

	for i in range(0,data.W_ttH.shape[0]):
		if (data.Z_ttH[i]*1e-3 < 130 and data.Z_ttH[i]*1e-3 > 120) :
			sumW = sumW + data.W_ttH[i];	
			if data.C_ttH[i] == 1:
				sumW_1 = sumW_1 + data.W_ttH[i];
			if data.C_ttH[i] == 2:
				sumW_2 = sumW_2 + data.W_ttH[i];
			if data.C_ttH[i] == 3:
				sumW_3 = sumW_3 + data.W_ttH[i];
			if data.C_ttH[i] == 4:
				sumW_4 = sumW_4 + data.W_ttH[i];

	print('Total Yields : %.5f  # Cat1 : %.5f # Cat2 : %.5f # Cat3 : %.5f # Cat4 : %.5f  # Sum : %.5f' % (sumW,sumW_1,sumW_2,sumW_3,sumW_4, sumW_1+sumW_2+sumW_3+sumW_4 ) );

	print('JJ')

	sumW = 0;
	sumW_1 = 0;
	sumW_2 = 0;
	sumW_3 = 0;
	sumW_4 = 0;

	for i in range(0,data.W_JJ.shape[0]):
		if (data.Z_JJ[i]*1e-3 < 130 and data.Z_JJ[i]*1e-3 > 120) :
			sumW = sumW + data.W_JJ[i];	
			if data.C_JJ[i] == 1:
				sumW_1 = sumW_1 + data.W_JJ[i];
			if data.C_JJ[i] == 2:
				sumW_2 = sumW_2 + data.W_JJ[i];
			if data.C_JJ[i] == 3:
				sumW_3 = sumW_3 + data.W_JJ[i];
			if data.C_JJ[i] == 4:
				sumW_4 = sumW_4 + data.W_JJ[i];

	print('Total Yields : %.5f  # Cat1 : %.5f # Cat2 : %.5f # Cat3 : %.5f # Cat4 : %.5f  # Sum : %.5f' % (sumW,sumW_1,sumW_2,sumW_3,sumW_4, sumW_1+sumW_2+sumW_3+sumW_4 ) );

	exit()
	"""
	dirname = "Plots_clf_"+str(dropout_rate)+"_"+str(learning_rate)+"_"+str(batch_size)+"_"+str(n_neurons)+"_"+str(nepochs);

	if not os.path.exists(dirname):
		os.mkdir(dirname)

	clf, history = train(nepochs,noutput,dropout_rate,learning_rate,batch_size,n_neurons)
	
	print("*** Plotting... ***")

	Y_Val_prob = clf.predict_proba(data.X_Val)

	auc_score=roc_auc_score(data.Y_Val, Y_Val_prob)

	print('Accuracy: %.2f ' % (auc_score*100) )

	HH  = clf.predict(data.X_HH)
	ttH = clf.predict(data.X_ttH)
	ZH  = clf.predict(data.X_ZH)
	JJ  = clf.predict(data.X_JJ)

	_, sig_acc = clf.evaluate(data.X_HH, data.Y_HH)
	print('Signal Accuracy: %.2f ' % (sig_acc*100))

	Z_max = plot_Z(HH,ttH,ZH,JJ,4,dirname)
	#plot_Z_DHH(HH,ttH,ZH,JJ,4,dirname)

	plot_D_HH(HH,ttH,ZH,JJ,dirname)
	"""	
	plot_Z(HH,ttH,ZH,JJ,5,dirname)
	plot_Z_DHH(HH,ttH,ZH,JJ,5,dirname)
	
	plot_Z(HH,ttH,ZH,JJ,1,dirname)
	plot_Z_DHH(HH,ttH,ZH,JJ,1,dirname)

	plot_Z(HH,ttH,ZH,JJ,2,dirname)
	plot_Z_DHH(HH,ttH,ZH,JJ,2,dirname)

	plot_Z(HH,ttH,ZH,JJ,3,dirname)
	plot_Z_DHH(HH,ttH,ZH,JJ,3,dirname)
	"""
	plot_model_history(history,dirname)
	plot_roc_curve(data.Y_Val, Y_Val_prob,dirname)

	clf_json = clf.to_json()
	with open("Model/"+dirname+"_"+str(Z_max)+"_model.json", "w") as json_file:
    		json_file.write(clf_json)
	clf.save_weights("Model/"+dirname+"_"+str(Z_max)+"_model.h5")

if __name__== '__main__':
	main()
	
